import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FlexLayoutModule} from '@angular/flex-layout';
import {RouterModule} from '@angular/router';
import {CrystalLightboxModule} from '@crystalui/angular-lightbox';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';

import {GalleryComponent} from './pages/gallery/gallery.component';
import {ListingComponent} from './pages/listing/listing.component';

@NgModule({
  declarations: [
    ListingComponent,
    GalleryComponent,
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    FlexLayoutModule,
    RouterModule,
    CrystalLightboxModule,
  ],
  exports: [
    ListingComponent,
    GalleryComponent,
  ]
})
export class CoreModule {
}
