import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {filter, map, switchMap} from 'rxjs/operators';
import {PhotoService} from '../../services/photo.service';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {

  deviceRatio = window?.devicePixelRatio ?? 1;

  deviceFullHeight = window?.screen?.availHeight ?? 1080;

  pictures: string[] = [];

  constructor(
    private activatedRoute: ActivatedRoute,
    private photoService: PhotoService,
  ) {
  }

  ngOnInit(): void {
    this.activatedRoute.paramMap
      .pipe(
        map(params => params.get('folder')),
        filter(folder => folder != null),
        switchMap(folder => this.photoService.listFiles(folder as string))
      )
      .subscribe(pictures => {
        this.pictures = pictures;
      });
  }

  resizeLink(imageUrl: string, height: number): string {
    return `https://images.weserv.nl/?url=${encodeURIComponent(imageUrl)}&h=${height}&dpr=${this.deviceRatio}`;
  }

}
