import {Component, OnInit} from '@angular/core';
import {PhotoService} from '../../services/photo.service';

@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.scss']
})
export class ListingComponent implements OnInit {

  folders: string[] = [];

  constructor(
    private photoService: PhotoService
  ) {
  }

  ngOnInit(): void {
    this.photoService.listFolders()
      .subscribe(folders => {
        this.folders = folders;
      });
  }

}
