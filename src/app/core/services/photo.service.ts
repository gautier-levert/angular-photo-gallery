import {Injectable} from '@angular/core';
import {AngularFireStorage} from '@angular/fire/storage';
import {from, Observable} from 'rxjs';
import {map, mergeMap, toArray} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PhotoService {

  constructor(
    private storage: AngularFireStorage
  ) {
  }

  listFolders(): Observable<string[]> {
    return this.storage.ref('').listAll()
      .pipe(
        map(res => {
          return res.prefixes.map(prefix => prefix.name);
        }),
      );
  }

  listFiles(folder: string): Observable<string[]> {
    return this.storage.ref(folder).listAll()
      .pipe(
        mergeMap(res => {
          return from(res.items);
        }),
        mergeMap(item => {
          return from(item.getDownloadURL());
        }),
        map(uri => uri.toString()),
        toArray(),
      );
  }
}
