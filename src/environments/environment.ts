// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyCAeaaD7JepELfjADBuoYYqAE_Rodtci2o',
    authDomain: 'gallerie-photo-68c69.firebaseapp.com',
    projectId: 'gallerie-photo-68c69',
    storageBucket: 'gallerie-photo-68c69.appspot.com',
    messagingSenderId: '937821922418',
    appId: '1:937821922418:web:e7fee399f3eb2629c7ceeb',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
